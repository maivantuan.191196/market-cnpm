import client from "../db/mysql.ts";

try {
  await client.execute(`
        CREATE TABLE products (
            id int(11) NOT NULL AUTO_INCREMENT,
            title varchar(255) NOT NULL,
            quantity integer NOT NULL,
            unit varchar(50) NOT NULL,
            price integer NOT NULL,
            image varchar(255) NOT NULL,
            contact varchar(255) NOT NULL,
            created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
    `);
} catch (err) {
  console.error(err);
}
