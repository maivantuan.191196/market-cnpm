import { Status } from "../deps.ts";

import Product from "../models/Product.ts";

export async function store(ctx: any) {
  const body = await ctx.request.body();

  const { productId, productCount } = await Product.create(body.value);

  ctx.response.status = Status.Created;
  ctx.response.type = "json";
  ctx.response.body = {
    status: "success",
    message: `${productCount} Product created in database`,
    data: {
      todo: {
        id: productId,
      },
    },
  };
}


export async function search(ctx: any) {
  const param = await ctx.request.url;
  const { products, titles, unit, quantity } = await Product.search(param);

  ctx.response.status = Status.OK;
  ctx.response.type = "json";
  ctx.response.body = {
    status: "success",
    message: 'Success',
    data: {
      titles: titles,
      quantity: quantity,
      unit: unit,
      products: products
    }
  };
}

