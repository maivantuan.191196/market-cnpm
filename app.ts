import { oakCors } from "https://deno.land/x/cors/mod.ts";
import app from "./api/server.ts";
import env from "./config/env.ts";

import products from "./routes/products.ts";

const host = env["HOST"] || "http://127.0.0.1";
const port = parseInt(env["PORT"]);

app.use(oakCors());
app.use(products.routes())

console.log(`app running -> ${host}:${port}`);
await app.listen({ port });
