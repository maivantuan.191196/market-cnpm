export function handleResponse(ctx: any, data: any) {
  if (data) {
    return ctx.response.body = {
      isSuccess: true,
      statusCode: 200,
      data,
    }
  }

  return ctx.response.body = {
    isSuccess: false,
    statusCode: 400,
    message: 'Bad request',
  }
}