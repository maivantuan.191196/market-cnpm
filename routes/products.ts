import { Router } from "../deps.ts";

import { store, search } from "../controllers/products.ts";

const router = new Router();

router.post("/products", store)
router.get("/products/search", search)


export default router;
