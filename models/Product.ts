import { slugify } from "../deps.ts";
import env from "../config/env.ts";
import ky from 'https://unpkg.com/ky/index.js';

import client from "../db/mysql.ts";

const tableName = "products";

export interface SearchingProducts {
  titles: string[],
  quantity: any,
  unit: string,
  page: any
}

class Product {
  public static DEFAULT_UNIT = "other";
  public static LIMIT = 35;
  public static PRODUCT_NAME_ENTITY = "product_name";
  public id: number;
  public title: string;
  public quantity: number;
  public unit: string;
  public status: number
  public price: number;
  public image: string;
  public contact: string;
  public createdAt: string;

  constructor(
    id: number,
    title: string,
    quantity: number,
    unit: string,
    status: number,
    price: number,
    image: string,
    contact: string,
    createdAt: string,
  ) {
    this.id = id;
    this.title = title;
    this.quantity = quantity;
    this.unit = unit;
    this.status = status;
    this.price = price;
    this.image = image;
    this.contact = contact;
    this.createdAt = createdAt;
  }

  static async all() {
    return (await client.execute("SELECT * FROM ??", [tableName])).rows;
  }

  static async create(params: any) {
    let titles = (params.title).split(', ')
    let {quantity, unit, price, contact, image} = params;
    if (image == undefined) {
        image = "Test"
    }
    if (titles.length >= 3) {
        titles = titles.slice(0, 3)
    }
    const result = await client.execute(
        "INSERT INTO ?? (title, quantity, unit, price, image, contact) VALUES (?, ?, ?, ?, ?, ?)",
        [tableName, titles[0], quantity, unit, price, image, contact],
    );
    await this.saveEntity(titles[0], titles)

    return { productId: result.lastInsertId, productCount: result.affectedRows };
  }

  static async searchProducts({ titles, unit, quantity, page }: SearchingProducts) {
    if (!titles || !titles.length || !unit || !quantity) {
      return []
    }

    let skip = (page - 1) * Product.LIMIT;

    if (unit != Product.DEFAULT_UNIT) {
        const query: any = await client.execute(
            `SELECT * FROM products
       WHERE title IN (?) AND quantity >= ? AND unit = ? limit ? offset ?
      `, [titles, quantity, unit, Product.LIMIT, skip]
        )
        return query.rows;
    }

      const query: any = await client.execute(
          `SELECT * FROM products
       WHERE title IN (?) AND quantity >= ? limit ? offset ?
      `, [titles, quantity, Product.LIMIT, skip]
      )

    return query.rows;
  }

  public static async search(params: any) {
    const text = params.searchParams.has('text') ? params.searchParams.get('text') : "";
    let result = await Product.detachProduct(text);
    let page = params.searchParams.has('page') ? params.searchParams.get('page') : 1;

      const products = await Product.searchProducts({
        titles: result.titles,
        unit: result.unit,
        quantity: result.quantity,
        page: page
    });

    return {
      products: products,
      quantity: result.quantity,
      unit: result.unit,
      titles: result.titles
    };
  }

  public static async detachProduct(text: string) {
    try {
      const response = await fetch(`${env['DIALOGFLOW_URL']}/query?v=20150910`, {
        method: 'POST',
        body: JSON.stringify({
          "lang": "en",
          "query": text,
          "sessionId": "12345",
          "timezone": "America/New_York"
        }),
        headers: {
          'content-type': 'application/json',
          'Authorization': `Bearer ${env['DIALOGFLOW_KEY']}`
        }
      });

      const parsed = await response.json();

      let data: any = parsed.result.parameters;
      if (data !== undefined) {
          let titles = data.product_name;
          let unit = data.product_unit == "" ? Product.DEFAULT_UNIT  : data.product_unit;
          let quantity = data.product_quantity == "" ? "1" : data.product_quantity;
          return {
              'titles': titles,
              'unit': unit,
              'quantity': quantity
          };
      }

      throw new Error("Not found")

    } catch (err) {
      return {
            'titles': [],
            'unit': "",
            'quantity': ""
        };
    }
  }

    public static async saveEntity(text: string, keyWords: string[]) {
        try {
            const response = await fetch(`${env['DIALOGFLOW_URL']}/entities/97d59e9f-db6d-44d2-80ac-ef9dace949dd/entries?v=20150910`, {
                method: 'PUT',
                body: JSON.stringify({
                    "synonyms": keyWords,
                    "value": text
                }),
                headers: {
                    'content-type': 'application/json',
                    'Authorization': `Bearer ${env['DIALOGFLOW_DEV_KEY']}`
                }
            });
        } catch (err) {
           console.log(err);
        }
    }
}

export default Product;
